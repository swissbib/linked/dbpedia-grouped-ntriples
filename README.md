## Dbpedia Group Ntriples

A flink job to sort and filter dbpedia n-triples files.

### Deployment

1. In `sb-ust1:/swissbib_index/apps` do `git clone https://gitlab.com/swissbib/linked/dbpedia-grouped-ntriples.git`
2. `cd dbpedia-grouped-ntriples`
3. `./deploy.sh`

### Run

In `/swissbib_index/apps/flink`

`./bin/flink run /swissbib_index/apps/dbpedia-grouped-ntriples-assembly-*.jar --conf /swissbib_index/nas/dbpedia/sort/app.properties &`