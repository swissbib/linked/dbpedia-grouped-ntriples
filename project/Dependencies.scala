/*
 * viaf-grouped-ntriples: Groups unordered Ntriples by subject and publishes them in a Kafka topic
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

import sbt._

object Dependencies {
  lazy val flinkVersion = "1.9.0"
  lazy val kafkaVersion = "2.1.1"
  lazy val log4jVersion = "2.11.2"
  lazy val slf4jVersion = "1.8.0-beta4"

  lazy val configTypesafe = "com.typesafe" % "config" % "1.3.4"
  lazy val flinkScala = "org.apache.flink" %% "flink-scala" % flinkVersion
  lazy val flinkStreamingScala = "org.apache.flink" %% "flink-streaming-scala" % flinkVersion
  lazy val kafkaClients = "org.apache.kafka" % "kafka-clients" % kafkaVersion
  lazy val kafkaMetadataWrapper = "org.swissbib" % "kafka-metadata-wrapper" % "2.2.1"
  lazy val log4jApi = "org.apache.logging.log4j" % "log4j-api" % log4jVersion
  lazy val log4jApiScala = "org.apache.logging.log4j" %% "log4j-api-scala" % "11.0"
  lazy val log4jCore = "org.apache.logging.log4j" % "log4j-core" % log4jVersion
  lazy val log4jSlf4jImpl = "org.apache.logging.log4j" % "log4j-slf4j18-impl" % log4jVersion
}
