import Dependencies._

ThisBuild / resolvers ++= Seq(
  "Apache Development Snapshot Repository" at "https://repository.apache.org/content/repositories/snapshots/",
  "kafka-metadata-wrapper" at "https://gitlab.com/api/v4/projects/11507450/packages/maven",
  Resolver.mavenLocal
)
ThisBuild / organization := "org.swissbib"
ThisBuild / organizationName := "Project Swissbib"
ThisBuild / scalaVersion := "2.12.8"

lazy val root = (project in file("."))
  .settings(
    name := "dbpedia-grouped-ntriples",
    version := "1.0.3",
    libraryDependencies ++= Seq(
      configTypesafe,
      flinkScala % "provided",
      flinkStreamingScala % "provided",
      kafkaClients,
      kafkaMetadataWrapper,
      log4jApi,
      log4jApiScala,
      log4jCore,
      log4jSlf4jImpl,
    )
  )

assembly / mainClass := Some("org.swissbib.Job")

// make run command include the provided dependencies
Compile / run := Defaults.runTask(
  Compile / fullClasspath,
  Compile / run / mainClass,
  Compile / run / runner
).evaluated

// stays inside the sbt console when we press "ctrl-c" while a Flink programme executes with "run" or "runMain"
Compile / run / fork := true
Global / cancelable := true

// exclude Scala library from assembly
assembly / assemblyOption := (assembly / assemblyOption).value.copy(includeScala = false)
