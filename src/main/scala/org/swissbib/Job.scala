/*
 * viaf-grouped-ntriples: Groups unordered Ntriples by subject and publishes them in a Kafka topic
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import java.util.Properties

import org.apache.flink.api.java.utils.ParameterTool
import org.apache.flink.api.scala.ExecutionEnvironment
import org.apache.flink.configuration.Configuration
import org.apache.flink.streaming.api.scala._
import org.apache.logging.log4j.scala.Logging

import scala.io.Source
import scala.util.matching.Regex

object Job extends Logging {
  val patternLA: Regex = "@[^\"]+$".r
  val patternId: Regex = "^<(http://((fr|en|de|it).)?dbpedia.org/resource/.*?)>".r

  def main(args: Array[String]): Unit = {
    val env = ExecutionEnvironment.getExecutionEnvironment
    val params = ParameterTool.fromArgs(args)
    val conf = new Properties
    val path = params.get("config")
    if (path == null) {
      logger.error("Pass a configuration file as path to --config!")
      System.exit(1)
    }
    conf.load(Source.fromFile(path).bufferedReader())
    logger.info("Loading config from " + params.get("config", "app.properties"))
    env.setParallelism(conf.getProperty("flink.parallelism", "3").toInt)
    env.getConfig.setGlobalJobParameters(params)

    val parameters = new Configuration
    parameters.setBoolean("recursive.file.enumeration", true)
    val stream = env.readTextFile(conf.getProperty("flink.input.file"))
      .withParameters(parameters)

    val filtered = stream
      .filter(!isMetaStatement(_))
      .filter(!isComment(_))
      .filter(!isValue(_))
      //.filter(t => !(isPropertyWithRequiredLanguageTag(t) && isLiteralWithoutLanguageAnnotation(t)))

    filtered
      .map(t => (selectKey(t), t))
      .groupBy(0)
      .reduce((e, a) => (a._1, a._2 + "\n" + e._2))
      .map(x => new SbMetadataModel().setData(x._2).setMessageKey(x._1))
      .output(new KafkaSink(conf))

    env.execute(conf.getProperty("flink.job.name"))
  }

  def isComment(triple: String): Boolean = triple.contains("http://www.w3.org/2000/01/rdf-schema#comment")

  def isValue(triple: String): Boolean = triple.contains("http://www.w3.org/1999/02/22-rdf-syntax-ns#value")

  def isLiteralWithoutLanguageAnnotation(triple: String): Boolean = patternLA.findFirstIn(triple).isEmpty

  def isMetaStatement(triple: String): Boolean = patternId.findFirstIn(triple).isEmpty

  def isPropertyWithRequiredLanguageTag(t: String): Boolean = Set(
    "http://schema.org/name",
    "http://schema.org/description",
    "http://www.w3.org/2004/02/skos/core#prefLabel"
  ).contains(t)

  def selectKey(triple: String): String = {
    patternId.findFirstMatchIn(triple).get.group(1)
  }
}
