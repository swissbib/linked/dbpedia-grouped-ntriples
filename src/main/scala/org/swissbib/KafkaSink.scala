/*
 * viaf-grouped-ntriples: Groups unordered Ntriples by subject and publishes them in a Kafka topic
 * Copyright (C) 2019  Project Swissbib <http://swissbib.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.swissbib

import java.util.Properties

import org.apache.flink.api.common.io.OutputFormat
import org.apache.flink.configuration.Configuration
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.logging.log4j.scala.Logging

class KafkaSink(val conf: Properties) extends OutputFormat[SbMetadataModel] with Logging {
  @transient var producer: KafkaProducer[String, SbMetadataModel] = _

  override def configure(parameters: Configuration): Unit = {
  }

  override def open(taskNumber: Int, numTasks: Int): Unit = {
    logger.info(s"Opening output task number $taskNumber of $numTasks tasks")
    val props: Properties = new Properties()
    props.put("bootstrap.servers", conf.getProperty("kafka.bootstrap.server"))
    props.put("key.serializer",
      "org.apache.kafka.common.serialization.StringSerializer")
    props.put("value.serializer",
      "org.swissbib.SbMetadataSerializer")
    props.put("acks", "all")

    producer = new KafkaProducer[String, SbMetadataModel](props)
  }

  override def writeRecord(record: SbMetadataModel): Unit = {
    val id = record.getMessageKey
    record.setMessageKey(null)
    record.setMessageDate(conf.getProperty("flink.input.date"))
    val rec = new ProducerRecord[String, SbMetadataModel](conf.getProperty("kafka.topic"), id, record)
    val metadata = producer.send(rec)
    logger.trace(s"sent record(key=${rec.key()} value=${rec.value()}) meta(partition=${metadata.get().partition()}, offset=${metadata.get.offset()})\n")
  }

  override def close(): Unit = producer.close()
}
